using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendasContext _context;

        public VendasController(VendasContext context)
        {
            _context = context;
        }

        // Endpoint para registrar a venda
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if(venda.Itens.Count == 0 || venda.Itens == null)
                return BadRequest(new { Erro = "A venda deve possuir 1 ou mais itens" });

            venda.StatusVenda = EnumStatusVenda.AguardandoPagamento;
            _context.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        // Endpoint para buscar venda por ID
        [HttpGet("{id}")]
        public IActionResult BuscarVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();

            return Ok(venda);
        }

        // Endpoint para atualizar o status da venda
        [HttpPut("{id}")]
        public IActionResult AtualizarStatusDaVenda(int id, EnumStatusVenda statusVenda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.StatusVenda == EnumStatusVenda.AguardandoPagamento 
                && statusVenda == EnumStatusVenda.PagamentoAprovado
                || statusVenda == EnumStatusVenda.Cancelada)
            {
                vendaBanco.StatusVenda = statusVenda;
            }

            if (vendaBanco.StatusVenda == EnumStatusVenda.PagamentoAprovado 
                && statusVenda == EnumStatusVenda.EnviadoParaTransportadora
                || statusVenda == EnumStatusVenda.Cancelada)
            {
                vendaBanco.StatusVenda = statusVenda;
            }

            if (vendaBanco.StatusVenda == EnumStatusVenda.EnviadoParaTransportadora
                && statusVenda == EnumStatusVenda.Entregue)
            {
                vendaBanco.StatusVenda = statusVenda;
            }
            

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

    }
}