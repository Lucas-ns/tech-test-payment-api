using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }

        public List<Produto> Itens { get; set; }

        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
    }
}